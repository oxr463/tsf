# tsf 

_Terminal Safe Font._

## Motivation

The purpose for this project is to provide a terminal safe font for the
[eXtended Window Manager (xwm)](https://gitlab.com/oxr463/xwm) project.

According to the terms of the [SIL Open Font License][OFL-1.1], any derivative works may
not use the original font name. Hence, *tsf* has been chosen as the Reserved Font
Name going forward.

## Acknowledgement

Based on the original [Terminus Font](http://terminus-font.sourceforge.net).

Converted to TrueType Format [(TTF)][TTF] via [Tilman Blumenbach's mkttf](https://github.com/Tblue/mkttf).

## License

This font is licensed under the [SIL Open Font License][OFL-1.1].

See [COPYING](COPYING) file for copyright and license details.

## Reference

- [TrueType - Wikipedia][TTF]

## See Also

- [3270font: A font for the nostalgic](https://github.com/rbanffy/3270font)

- [Hack](https://sourcefoundry.org/hack)

- [Source Code Pro](https://adobe-fonts.github.io/source-code-pro)

[OFL-1.1]: https://spdx.org/licenses/OFL-1.1.html
[TTF]: https://en.wikipedia.org/wiki/TrueType
